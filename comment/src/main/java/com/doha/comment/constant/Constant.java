package com.doha.comment.constant;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class Constant {
    public static final String COMMENT_NOT_FOUND = "COMMENT_NOT_FOUND";
    public static final String POSTS_NOT_FOUND = "POSTS_NOT_FOUND";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String SUCCESSFUL = "SUCCESSFUL";
    public static final String VALIDATION_ERROR = "VALIDATION_ERROR";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
}
