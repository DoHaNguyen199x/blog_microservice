package com.doha.comment.exception;

public class PostsNotFound extends RuntimeException {
    public PostsNotFound(String message) {
        super(message);
    }
}
