package com.doha.comment.controller;

import com.doha.comment.dto.Request.CommentRequest;
import com.doha.comment.dto.Response.CommentResponse;
import com.doha.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("api/comment/")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/")
    public CommentResponse save(@RequestBody CommentRequest commentRequest) {
        return commentService.save(commentRequest);
    }

    @PutMapping("/{id}")
    public CommentResponse update(@PathVariable(name = "id") Long id,
                                  @RequestBody CommentRequest commentRequest) {
        return commentService.update(id, commentRequest);
    }

    @GetMapping("/{id}")
    public CommentResponse findById(@PathVariable(name = "id") Long id) {
        return commentService.findById(id);
    }

    @GetMapping("/{n}/{s}")
    public Page<CommentResponse> findAll(@PathVariable(name = "n") Integer n,
                                         @PathVariable(name = "s") Integer s) {
        return commentService.findAll(PageRequest.of(n, s));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
        return commentService.delete(id);
    }
}
