package com.doha.comment.dto.Request;


import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class CommentRequest {
    private Long id;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private String comment;
    private Long users;
    private Long posts;
}
