package com.doha.comment.dto.Response;

import com.doha.comment.entity.Posts;
import lombok.Builder;
import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class PostsResponse {
    private Long id;
    private String title;

    public static PostsResponse fromEntity(Posts posts) {
        return PostsResponse.builder()
                .id(posts.getId())
                .title(posts.getTitle())
                .build();
    }
}
