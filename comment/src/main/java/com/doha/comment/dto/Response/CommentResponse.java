package com.doha.comment.dto.Response;

import com.doha.comment.entity.Comments;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class CommentResponse {
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime modifiedAt;
    private String comment;
    private UserResponse users;
    private PostsResponse posts;

    public static CommentResponse fromEntity(Comments comments) {
        return CommentResponse.builder()
                .id(comments.getId())
                .createdAt(comments.getCreatedAt())
                .modifiedAt(comments.getModifiedAt())
                .comment(comments.getComment())
                .users(UserResponse.fromEntity(comments.getUsers()))
                .posts(PostsResponse.fromEntity(comments.getPosts()))
                .build();
    }
}
