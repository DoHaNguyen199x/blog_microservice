package com.doha.comment.entity.elses;

public enum ERank {
    TRAINEE,
    FRESHER,
    JUNIOR,
    SENIOR,
    MANAGER
}
