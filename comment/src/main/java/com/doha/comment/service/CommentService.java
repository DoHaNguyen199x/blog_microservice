package com.doha.comment.service;


import com.doha.comment.dto.Request.CommentRequest;
import com.doha.comment.dto.Response.CommentResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface CommentService {
    public CommentResponse save(CommentRequest commentRequest);

    public CommentResponse update(Long id, CommentRequest commentRequest);

    public CommentResponse findById(Long id);

    public Page<CommentResponse> findAll(Pageable pageable);

    public ResponseEntity<?> delete(Long id);
}
