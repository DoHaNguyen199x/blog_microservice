package com.doha.comment.service.imp;

import com.doha.comment.constant.Constant;
import com.doha.comment.controller.ExceptionHandler.Message;
import com.doha.comment.dto.Request.CommentRequest;
import com.doha.comment.dto.Response.CommentResponse;
import com.doha.comment.entity.Comments;
import com.doha.comment.entity.Posts;
import com.doha.comment.entity.Users;
import com.doha.comment.exception.CommentNotFound;
import com.doha.comment.exception.PostsNotFound;
import com.doha.comment.exception.UserNotFound;
import com.doha.comment.repository.CommentRepository;
import com.doha.comment.repository.PostsRepository;
import com.doha.comment.repository.UserRepository;
import com.doha.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class CommentServiceImp implements CommentService {

    private final CommentRepository commentRepository;
    private final PostsRepository postsRepository;
    private final UserRepository userRepository;

    @Autowired
    public CommentServiceImp(CommentRepository commentRepository, PostsRepository postsRepository,
                             UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.postsRepository = postsRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public CommentResponse save(CommentRequest commentRequest) {
        Users users = userRepository.findById(commentRequest.getUsers()).orElseThrow(() -> {
            throw new UserNotFound("user does not exits !!!");
        });
        Posts posts = postsRepository.findById(commentRequest.getPosts()).orElseThrow(() -> {
            throw new PostsNotFound("posts does not exist !!!");
        });
        Comments comments = Comments.builder()
                .createdAt(LocalDateTime.now())
                .modifiedAt(LocalDateTime.now())
                .comment(commentRequest.getComment())
                .users(users)
                .posts(posts)
                .build();
        return CommentResponse.fromEntity(commentRepository.save(comments));
    }

    @Transactional
    @Override
    public CommentResponse update(Long id, CommentRequest commentRequest) {
        Comments update = commentRepository.findById(id).orElseThrow(() -> {
            throw new CommentNotFound("comment does not exist !!!");
        });
        update.setModifiedAt(LocalDateTime.now());
        update.setComment(commentRequest.getComment());
        return CommentResponse.fromEntity(commentRepository.save(update));
    }

    @Transactional(readOnly = true)
    @Override
    public CommentResponse findById(Long id) {
        return commentRepository.findById(id).map(CommentResponse::fromEntity).orElseThrow(() -> {
            throw new CommentNotFound("comment does not exist !!!");
        });
    }

    @Transactional(readOnly = true)
    @Override
    public Page<CommentResponse> findAll(Pageable pageable) {
        return commentRepository.findAll(pageable).map(CommentResponse::fromEntity);
    }

    @Transactional
    @Override
    public ResponseEntity<?> delete(Long id) {
        commentRepository.findById(id).orElseThrow(() -> {
            throw new CommentNotFound("comment does not exist !!!");
        });
        commentRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(Message.builder()
                .code(Constant.SUCCESSFUL)
                .time(LocalDateTime.now())
                .message("Delete successful comment !!!")
                .build());
    }
}
