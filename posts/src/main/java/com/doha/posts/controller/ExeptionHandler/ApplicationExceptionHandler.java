package com.doha.posts.controller.ExeptionHandler;

import com.doha.posts.constant.Constant;
import com.doha.posts.exception.GroupNotFound;
import com.doha.posts.exception.PostsNotFound;
import com.doha.posts.exception.UserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;
import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {GroupNotFound.class})
    public ResponseEntity<?> GroupNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.GROUP_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {PostsNotFound.class})
    public ResponseEntity<?> PostsNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.POSTS_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {UserNotFound.class})
    public ResponseEntity<?> UserNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.USER_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<?> Exception(Exception e) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Message.builder()
                        .code(Constant.INTERNAL_SERVER_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {ValidationException.class})
    public ResponseEntity<?> ValidationException(Exception e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.VALIDATION_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }
}
