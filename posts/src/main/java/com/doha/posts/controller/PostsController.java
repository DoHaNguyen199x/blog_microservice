package com.doha.posts.controller;

import com.doha.posts.dto.request.PostsRequest;
import com.doha.posts.dto.response.PostsResponse;
import com.doha.posts.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RequestMapping("api/posts/")
@RestController
public class PostsController {
    private final PostsService postsService;

    @Autowired
    public PostsController(PostsService postsService) {
        this.postsService = postsService;
    }

    @PostMapping("/")
    public PostsResponse save(@Valid @RequestBody PostsRequest postsRequest) {
        return postsService.save(postsRequest);
    }

    @PutMapping("/{id}")
    public PostsResponse update(@PathVariable(name = "id") Long aLong, @RequestBody PostsRequest postsRequest) {
        return postsService.update(aLong, postsRequest);
    }

    @GetMapping("/{id}")
    public PostsResponse findById(@PathVariable(name = "id") Long aLong) {
        return postsService.findById(aLong);
    }

    @GetMapping("/{n}/{s}")
    public Page<PostsResponse> findAll(@PathVariable(name = "n") Integer n
            , @PathVariable(name = "s") Integer s) {
        return postsService.findAll(PageRequest.of(n, s));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") Long aLong) {
        return postsService.delete(aLong);
    }

}
