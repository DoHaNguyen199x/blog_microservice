package com.doha.posts.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class PostsNotFound extends RuntimeException {
    public PostsNotFound(String message) {
        super(message);
    }
}
