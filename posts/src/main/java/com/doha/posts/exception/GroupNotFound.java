package com.doha.posts.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class GroupNotFound extends RuntimeException {
    public GroupNotFound(String message) {
        super(message);
    }
}
