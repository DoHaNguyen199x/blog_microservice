package com.doha.posts.entity.elses;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public enum EStatus {
    ACTIVATE,
    BANNED
}
