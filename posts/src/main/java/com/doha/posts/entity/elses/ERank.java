package com.doha.posts.entity.elses;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public enum ERank {
    TRAINEE,
    FRESHER,
    JUNIOR,
    SENIOR,
    MANAGER
}
