package com.doha.posts.dto.response;

import com.doha.posts.entity.Posts;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class PostsResponse {
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime modifiedAt;
    private String title;
    private String posts;
    private GroupResponse groups;
    private UserResponse users;

    public static PostsResponse fromEntity(Posts posts) {
        return PostsResponse.builder()
                .id(posts.getId())
                .createdAt(posts.getCreatedAt())
                .modifiedAt(posts.getModifiedAt())
                .title(posts.getTitle())
                .posts(posts.getPosts())
                .groups(GroupResponse.fromEntity(posts.getGroups()))
                .users(UserResponse.fromEntity(posts.getUsers()))
                .build();
    }
}
