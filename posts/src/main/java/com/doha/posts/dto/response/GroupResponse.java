package com.doha.posts.dto.response;


import com.doha.posts.entity.Groups;
import lombok.Builder;
import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class GroupResponse {
    private Long id;
    private String groupName;

    public static GroupResponse fromEntity(Groups groups) {
        return GroupResponse.builder()
                .id(groups.getId())
                .groupName(groups.getGroupName())
                .build();
    }
}
