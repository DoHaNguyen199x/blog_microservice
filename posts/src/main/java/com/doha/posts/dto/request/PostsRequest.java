package com.doha.posts.dto.request;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class PostsRequest {
    private Long id;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private String title;
    private String posts;
    private Long groups;
    private Long users;
}
