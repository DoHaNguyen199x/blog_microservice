package com.doha.posts.service.imp;

import com.doha.posts.constant.Constant;
import com.doha.posts.controller.ExeptionHandler.Message;
import com.doha.posts.dto.request.PostsRequest;
import com.doha.posts.dto.response.PostsResponse;
import com.doha.posts.entity.Groups;
import com.doha.posts.entity.Posts;
import com.doha.posts.entity.Users;
import com.doha.posts.exception.GroupNotFound;
import com.doha.posts.exception.PostsNotFound;
import com.doha.posts.exception.UserNotFound;
import com.doha.posts.repository.GroupsRepository;
import com.doha.posts.repository.PostsRepository;
import com.doha.posts.repository.UsersRepository;
import com.doha.posts.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Service
public class PostsServiceImp implements PostsService {
    private final PostsRepository postsRepository;
    private final UsersRepository usersRepository;
    private final GroupsRepository groupsRepository;

    @Autowired
    public PostsServiceImp(PostsRepository postsRepository, UsersRepository usersRepository,
                           GroupsRepository groupsRepository) {
        this.postsRepository = postsRepository;
        this.usersRepository = usersRepository;
        this.groupsRepository = groupsRepository;
    }

    @Transactional
    @Override
    public PostsResponse save(PostsRequest postsRequest) {
        Users users = usersRepository.findById(postsRequest.getUsers()).orElseThrow(() -> {
            throw new UserNotFound("user does not exist !!!");
        });
        Groups groups = groupsRepository.findById(postsRequest.getGroups()).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
        Posts posts = Posts.builder()
                .title(postsRequest.getTitle())
                .posts(postsRequest.getPosts())
                .createdAt(LocalDateTime.now())
                .modifiedAt(LocalDateTime.now())
                .users(users)
                .groups(groups)
                .build();
        return PostsResponse.fromEntity(postsRepository.save(posts));
    }

    @Transactional
    @Override
    public PostsResponse update(Long aLong, PostsRequest postsRequest) {
        Posts update = postsRepository.findById(aLong).orElseThrow(() -> {
            throw new PostsNotFound("posts does not exist !!!");
        });
        update.setModifiedAt(LocalDateTime.now());
        update.setTitle(postsRequest.getTitle());
        update.setPosts(postsRequest.getPosts());
        return PostsResponse.fromEntity(postsRepository.save(update));
    }

    @Transactional(readOnly = true)
    @Override
    public PostsResponse findById(Long aLong) {
        return postsRepository.findById(aLong).map(PostsResponse::fromEntity).orElseThrow(() -> {
            throw new PostsNotFound("posts does not exist !!!");
        });
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PostsResponse> findAll(Pageable pageable) {
        return postsRepository.findAll(pageable).map(PostsResponse::fromEntity);
    }

    @Transactional
    @Override
    public ResponseEntity<?> delete(Long aLong) {
        postsRepository.findById(aLong).orElseThrow(() -> {
            throw new PostsNotFound("posts does not exist !!!");
        });
        postsRepository.deleteById(aLong);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("Delete posts successful !!!!")
                        .build());
    }
}
