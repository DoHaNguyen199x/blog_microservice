package com.doha.posts.service;

import com.doha.posts.dto.request.PostsRequest;
import com.doha.posts.dto.response.PostsResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public interface PostsService {
    public PostsResponse save(PostsRequest req);

    public PostsResponse update(Long ids, PostsRequest req);

    public PostsResponse findById(Long ids);

    public Page<PostsResponse> findAll(Pageable pageable);

    public ResponseEntity<?> delete(Long ids);
}
