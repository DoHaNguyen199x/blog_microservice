package com.doha.posts.constant;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class Constant {
    public static final String SUCCESSFUL = "SUCCESSFUL";
    public static final String POSTS_NOT_FOUND = "POSTS_NOT_FOUND";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String GROUP_NOT_FOUND = "GROUP_NOT_FOUND";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    public static final String VALIDATION_ERROR = "VALIDATION_ERROR";
}
