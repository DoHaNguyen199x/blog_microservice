package com.doha.posts.repository;

import com.doha.posts.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {
}
