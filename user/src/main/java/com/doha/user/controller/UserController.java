package com.doha.user.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("/")
    public String Test() {
        return "user test !!!!";
    }
}
