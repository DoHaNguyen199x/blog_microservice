package com.doha.group.service.imp;

import com.doha.group.constant.Constant;
import com.doha.group.controller.ExceptionHandler.Message;
import com.doha.group.dto.Request.GroupRequest;
import com.doha.group.dto.Response.GroupResponse;
import com.doha.group.entity.Groups;
import com.doha.group.entity.Users;
import com.doha.group.exception.GroupError;
import com.doha.group.exception.GroupNotFound;
import com.doha.group.exception.UserNotFound;
import com.doha.group.repository.GroupRepository;
import com.doha.group.repository.UserRepository;
import com.doha.group.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Service
public class GroupServiceImp implements GroupService {
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    @Autowired
    public GroupServiceImp(UserRepository userRepository, GroupRepository groupRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
    }

    @Transactional
    @Override
    public GroupResponse save(GroupRequest groupRequest) {
        Users users = userRepository.findById(groupRequest.getUsers()).orElseThrow(() -> {
            throw new UserNotFound("user does not exist !!!");
        });
        Groups groups = Groups.builder()
                .createdAt(LocalDateTime.now())
                .modifiedAt(LocalDateTime.now())
                .description(groupRequest.getDescription())
                .groupName(groupRequest.getGroupName())
                .users(users)
                .listUser(Collections.emptySet())
                .build();
        return GroupResponse.fromEntity(groupRepository.save(groups));
    }

    @Transactional
    @Override
    public GroupResponse update(Long id, GroupRequest groupRequest) {
        Groups update = groupRepository.findById(id).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
        Users users = userRepository.findById(groupRequest.getUsers()).orElseThrow(() -> {
            throw new UserNotFound("user does not exist !!!");
        });
        update.setModifiedAt(LocalDateTime.now());
        update.setDescription(groupRequest.getDescription());
        update.setGroupName(groupRequest.getGroupName());
        update.setUsers(users);
        return GroupResponse.fromEntity(groupRepository.save(update));
    }

    @Transactional(readOnly = true)
    @Override
    public GroupResponse findById(Long id) {
        return groupRepository.findById(id).map(GroupResponse::fromEntity).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
    }

    @Transactional(readOnly = true)
    @Override
    public Page<GroupResponse> findAll(Pageable pageable) {
        return groupRepository.findAll(pageable).map(GroupResponse::fromEntity);
    }

    @Transactional
    @Override
    public GroupResponse addUserGroup(Long GroupId, GroupRequest groupRequest) {
        Groups groups = groupRepository.findById(GroupId).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
        Set<Users> usersSet = new HashSet<>();
        groupRequest.getListUser().forEach(aLong -> {
            Users users = userRepository.findById(aLong).orElseThrow(() -> {
                throw new UserNotFound("user does not exist !!!");
            });
            boolean check = usersSet.stream().filter(users1 -> users1.getId().equals(aLong)).isParallel();
            if (check) {
                throw new GroupError("users must not be the same !!!");
            }
            usersSet.add(users);
        });
        groups.setListUser(usersSet);
        return GroupResponse.fromEntity(groupRepository.save(groups));
    }

    @Transactional
    @Override
    public ResponseEntity<?> removeUserGroup(Long GroupId, GroupRequest groupRequest) {
        Groups groups = groupRepository.findById(GroupId).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
        groups.setListUser(groups.getListUser());
        groupRequest.getListUser().forEach(aLong -> {
            Users users = userRepository.findById(aLong).orElseThrow(() -> {
                throw new UserNotFound("user does not exist !!!");
            });
            groups.getListUser().remove(users);
        });
        Groups groupSave = groupRepository.save(groups);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message(GroupResponse.fromEntity(groupSave).toString())
                        .build());
    }

    @Transactional
    @Override
    public ResponseEntity<?> deleteGroup(Long id) {
        groupRepository.findById(id).orElseThrow(() -> {
            throw new GroupNotFound("group does not exist !!!");
        });
        groupRepository.deleteById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("Delete Group Successful !!!")
                        .build());
    }
}
