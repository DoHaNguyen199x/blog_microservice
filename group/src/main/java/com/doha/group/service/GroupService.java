package com.doha.group.service;

import com.doha.group.dto.Request.GroupRequest;
import com.doha.group.dto.Response.GroupResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public interface GroupService {
    public GroupResponse save(GroupRequest groupRequest);

    public GroupResponse update(Long id, GroupRequest groupRequest);

    public GroupResponse findById(Long id);

    public Page<GroupResponse> findAll(Pageable pageable);

    public GroupResponse addUserGroup(Long GroupId, GroupRequest groupRequest);

    public ResponseEntity<?> removeUserGroup(Long GroupId, GroupRequest groupRequest);

    public ResponseEntity<?> deleteGroup(Long id);
}
