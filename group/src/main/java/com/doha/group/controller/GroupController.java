package com.doha.group.controller;

import com.doha.group.dto.Request.GroupRequest;
import com.doha.group.dto.Response.GroupResponse;
import com.doha.group.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("api/group/")
public class GroupController {
    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping("/")
    public GroupResponse save(@RequestBody GroupRequest groupRequest) {
        return groupService.save(groupRequest);
    }

    @PutMapping("/{id}")
    public GroupResponse update(@PathVariable(name = "id") Long id,
                                @RequestBody GroupRequest groupRequest) {
        return groupService.update(id, groupRequest);
    }

    @GetMapping("/{id}")
    public GroupResponse findById(@PathVariable(name = "id") Long id) {
        return groupService.findById(id);
    }

    @GetMapping("/{n}/{s}")
    public Page<GroupResponse> findAll(@PathVariable(name = "n") Integer n,
                                       @PathVariable(name = "s") Integer s) {
        return groupService.findAll(PageRequest.of(n, s));
    }

    @PostMapping("/adduser/{id}")
    public GroupResponse addUserGroup(@PathVariable(name = "id") Long GroupId,
                                      @RequestBody GroupRequest groupRequest) {
        return groupService.addUserGroup(GroupId, groupRequest);
    }

    @PostMapping("/removeuser/{id}")
    public ResponseEntity<?> removeUserGroup(@PathVariable(name = "id") Long GroupId,
                                             @RequestBody GroupRequest groupRequest) {
        return groupService.removeUserGroup(GroupId, groupRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteGroup(@PathVariable(name = "id") Long id) {
        return groupService.deleteGroup(id);
    }
}
