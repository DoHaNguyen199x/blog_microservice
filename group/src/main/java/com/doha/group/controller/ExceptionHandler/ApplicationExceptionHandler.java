package com.doha.group.controller.ExceptionHandler;

import com.doha.group.constant.Constant;
import com.doha.group.exception.GroupError;
import com.doha.group.exception.GroupNotFound;
import com.doha.group.exception.UserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {GroupNotFound.class})
    public ResponseEntity<?> GroupNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.GROUP_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {UserNotFound.class})
    public ResponseEntity<?> UserNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.USER_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {GroupError.class})
    public ResponseEntity<?> GroupError(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.GROUP_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<?> Exception(Exception e) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Message.builder()
                        .code(Constant.INTERNAL_SERVER_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }
}
