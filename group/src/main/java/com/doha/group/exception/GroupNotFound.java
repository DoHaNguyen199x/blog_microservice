package com.doha.group.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class GroupNotFound extends RuntimeException {
    public GroupNotFound(String message) {
        super(message);
    }
}
