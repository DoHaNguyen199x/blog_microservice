package com.doha.group.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class GroupError extends RuntimeException {
    public GroupError(String message) {
        super(message);
    }
}
