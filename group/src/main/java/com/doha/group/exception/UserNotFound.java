package com.doha.group.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class UserNotFound extends RuntimeException {
    public UserNotFound(String message) {
        super(message);
    }
}
