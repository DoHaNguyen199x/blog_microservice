package com.doha.group.repository;

import com.doha.group.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
}
