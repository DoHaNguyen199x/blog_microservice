package com.doha.group.repository;

import com.doha.group.entity.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface GroupRepository extends JpaRepository<Groups, Long> {
}
