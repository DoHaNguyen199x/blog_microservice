package com.doha.group.dto.Request;

import com.doha.group.entity.Users;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class GroupRequest {
    private Long id;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private String description;
    private String groupName;
    private Long users;
    private Set<Long> listUser;
}
