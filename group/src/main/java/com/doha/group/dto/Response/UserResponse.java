package com.doha.group.dto.Response;

import com.doha.group.entity.Users;
import lombok.Builder;
import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class UserResponse {
    private Long id;
    private String fullName;
    private String email;

    public static UserResponse fromEntity(Users users) {
        return UserResponse.builder()
                .id(users.getId())
                .fullName(users.getFullName())
                .email(users.getEmail())
                .build();
    }
}
