package com.doha.group.dto.Response;

import com.doha.group.entity.Groups;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
public class GroupResponse {
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createdAt;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime modifiedAt;
    private String description;
    private String groupName;
    private UserResponse users;
    private List<UserResponse> listUser;

    public static GroupResponse fromEntity(Groups groups) {
        return GroupResponse.builder()
                .id(groups.getId())
                .createdAt(groups.getCreatedAt())
                .modifiedAt(groups.getModifiedAt())
                .description(groups.getDescription())
                .groupName(groups.getGroupName())
                .users(UserResponse.fromEntity(groups.getUsers()))
                .listUser(groups.getListUser()
                        .stream()
                        .map(UserResponse::fromEntity)
                        .collect(Collectors.toList()))
                .build();
    }
}
