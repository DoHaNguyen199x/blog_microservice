package com.doha.group.entity;

import com.doha.group.entity.elses.ERank;
import com.doha.group.entity.elses.EStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private String fullName;
    private String email;
    private String phone;
    @Enumerated(EnumType.STRING)
    private ERank ranks;
    @Enumerated(EnumType.STRING)
    private EStatus status;
}
