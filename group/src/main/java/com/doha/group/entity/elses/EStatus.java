package com.doha.group.entity.elses;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public enum EStatus {
    ACTIVATE,
    BANNED
}
