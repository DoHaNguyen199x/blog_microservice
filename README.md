@Blog Microservice
--------
> The project is underway . . .
-----
@ Operating model:

![img.png](img.png)

----
@ Setup keycloak (Quarkus) :

- open folder keycloak\conf , open keycloak.conf ( config database keycloak ).
- open terminal cd folder keycloak\bin run
  ![img_1.png](img_1.png)
- open browser run http://localhost:8080 create account.
- login keycloak
  ![img_2.png](img_2.png)
- create realm :

![img_3.png](img_3.png)

![img_4.png](img_4.png)

- AddClient: click client -> create client

![img_5.png](img_5.png)

![img_6.png](img_6.png)

- click next -> save.
- RootUrl(gateway port)

![img_7.png](img_7.png)
- add role : click realm role -> create role -> save.

![img_8.png](img_8.png)
- addUser : click User -> create new user

![img_9.png](img_9.png)
- set password user : click Credentials -> click set password.

![img_10.png](img_10.png)

- Set Role User : click Role mapping -> assign role

![img_11.png](img_11.png)

- api : click realm settings -> OpenID Endpoint Configuration

![img_12.png](img_12.png)
